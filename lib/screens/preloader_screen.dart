import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:test_doover/blocs/authorization/authorization_bloc.dart';
import 'package:test_doover/screens/home_screen.dart';
import 'package:test_doover/screens/login_screen.dart';

class PreloaderScreen extends StatelessWidget {
  static const routeName = '/';

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthorizationBloc, AuthorizationState>(
      listener: (context, state) {
        if (state is AuthorizationUnauthorized) {
          Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);
        }
        if (state is AuthorizationAuthorized) {
          Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
        }
      },
      child: Container(
        color: CupertinoColors.white,
        child: Center(
          child: SpinKitCircle(
            color: CupertinoColors.activeBlue,
          ),
        ),
      ),
    );
  }
}
