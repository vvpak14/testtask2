import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_doover/blocs/catalog/catalog_bloc.dart';
import 'package:test_doover/constants.dart';
import 'package:test_doover/widgets/catalog_cart.dart';

class CartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('Корзина'),
        trailing: GestureDetector(
          onTap: () {
            BlocProvider.of<CatalogBloc>(context).add(CatalogResetCounts());
          },
          child: Text(
            'Очистить',
            style: TextStyle(
              fontSize: 16.0,
              color: CupertinoColors.systemRed,
            ),
          ),
        ),
      ),
      child: SafeArea(
        child: BlocBuilder<CatalogBloc, CatalogState>(
          builder: (context, state) {
            if (state is CatalogLoaded) {
              var selectedCatalogItems = state.catalogItems
                  .where(
                      (element) => element.count != null && element.count > 0)
                  .toList();
              var count = selectedCatalogItems.fold(0,
                  (previousValue, element) => previousValue += element.count);
              var sum = selectedCatalogItems.fold(
                  0,
                  (previousValue, element) =>
                      previousValue += element.unitPrice * element.count);
              return Column(
                children: [
                  Expanded(
                    flex: 4,
                    child: ListView.builder(
                      itemCount: selectedCatalogItems.length,
                      itemBuilder: (context, index) {
                        var catalog = selectedCatalogItems.elementAt(index);
                        return CatalogCard(
                          catalog: catalog,
                          key: UniqueKey(),
                        );
                      },
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.all(16.0),
                      color: CupertinoColors.white,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('Общая сумма $sum',
                                  style: kCatalogTitleTextStyle),
                              Text('$count вещи ',
                                  style: kCatalogTitleTextStyle),
                            ],
                          ),
                          CupertinoButton(
                            onPressed: () {},
                            child: Text('Оформить'),
                            color: CupertinoColors.activeBlue,
                          )
                        ],
                      ),
                    ),
                  )
                ],
              );
            }
            return Container(
              child: Text('Корзина пуста'),
            );
          },
        ),
      ),
    );
  }
}
