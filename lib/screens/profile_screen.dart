import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_doover/blocs/authorization/authorization_bloc.dart';

class ProfileScreen extends StatelessWidget {
  static const routeName = '/profile';
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: BlocBuilder<AuthorizationBloc, AuthorizationState>(
          builder: (context, state) {
            if (state is AuthorizationAuthorized) {
              return Text(state.profile.login);
            }
            return Text('Профиль');
          },
        ),
      ),
      child: Container(
        child: ListView(
          children: [
            GestureDetector(
              onTap: () {
                BlocProvider.of<AuthorizationBloc>(context)
                    .add(AuthorizationLogout());
              },
              child: Container(
                decoration: BoxDecoration(color: CupertinoColors.white),
                padding: EdgeInsets.all(16.0),
                child: Text(
                  'Выйти',
                  style: TextStyle(
                    color: CupertinoColors.systemRed,
                    fontSize: 16.0,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
