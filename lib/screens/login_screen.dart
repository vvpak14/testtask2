import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:test_doover/blocs/authorization/authorization_bloc.dart';
import 'package:test_doover/blocs/login/login_bloc.dart';
import 'package:test_doover/constants.dart';
import 'package:test_doover/screens/home_screen.dart';

class LoginScreen extends StatefulWidget {
  static const routeName = '/login';
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _loginController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('Войти'),
      ),
      child: BlocListener<AuthorizationBloc, AuthorizationState>(
        listener: (context, state) {
          if (state is AuthorizationAuthorized) {
            Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
          }
        },
        child: SafeArea(
          child: Container(
            child: Form(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  CupertinoTextField(
                    controller: _loginController,
                    style: kLoginTextInputStyle,
                    placeholder: 'Логин',
                  ),
                  CupertinoTextField(
                    controller: _passwordController,
                    style: kLoginTextInputStyle,
                    placeholder: 'Пароль',
                  ),
                  BlocBuilder<LoginBloc, LoginState>(
                    builder: (context, state) {
                      if (state is LoginLoading) {
                        return SpinKitCircle(
                          color: CupertinoColors.activeBlue,
                        );
                      }
                      if (state is LoginError) {
                        return Text(state.errorString);
                      }
                      return SizedBox(
                        height: 50,
                      );
                    },
                  ),
                  CupertinoButton(
                    onPressed: () {
                      BlocProvider.of<LoginBloc>(context).add(
                        LoginButtonPress(
                          username: _loginController.text.trim(),
                          password: _passwordController.text.trim(),
                        ),
                      );
                    },
                    color: CupertinoColors.activeBlue,
                    child: Text(
                      'Войти',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
