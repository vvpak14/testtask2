import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_doover/blocs/authorization/authorization_bloc.dart';
import 'package:test_doover/blocs/categories/categories_bloc.dart';
import 'package:test_doover/constants.dart';
import 'package:test_doover/models/catalog_screen_arguments.dart';
import 'package:test_doover/models/category_model.dart';
import 'package:test_doover/repositories/categories_repository.dart';
import 'package:test_doover/screens/catalog_screen.dart';
import 'package:test_doover/widgets/error_screen.dart';

class CategoriesScreen extends StatefulWidget {
  static const routeName = '/categories';
  @override
  _CategoriesScreenState createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen> {
  CategoriesRepository _categoriesRepository = CategoriesRepository();

  String searchQuery = '';

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CategoriesBloc(
        authorizationBloc: BlocProvider.of<AuthorizationBloc>(context),
        categoriesRepository: _categoriesRepository,
      )..add(CategoriesLoad()),
      child: SafeArea(
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: Column(
            children: [
              CupertinoTextField(
                style: TextStyle(color: CupertinoColors.black),
                placeholder: 'Найти вещь',
                onChanged: (value) {
                  setState(() {
                    searchQuery = value;
                  });
                },
              ),
              Expanded(
                child: BlocBuilder<CategoriesBloc, CategoriesState>(
                  builder: (context, state) {
                    if (state is CategoriesLoading) {
                      return CupertinoActivityIndicator();
                    }
                    if (state is CategoriesLoaded) {
                      var categories = state.categories.where(
                        (element) =>
                            element.name
                                .toLowerCase()
                                .contains(searchQuery.toLowerCase()) ||
                            element.description
                                .toLowerCase()
                                .contains(searchQuery.toLowerCase()),
                      );

                      return ListView.builder(
                        itemCount: categories.length,
                        itemBuilder: (context, index) {
                          var category = categories.elementAt(index);

                          return CategoryItem(category: category);
                        },
                      );
                    }
                    if (state is CategoriesError) {
                      return ErrorScreen(
                        errorText: state.errorText,
                      );
                    }
                    return ErrorScreen(
                      errorText: 'Unknown error',
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CategoryItem extends StatelessWidget {
  const CategoryItem({
    Key key,
    @required this.category,
  }) : super(key: key);

  final CategoryModel category;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(
          context,
          CatalogScreen.routeName,
          arguments: CatalogScreenArguments(category: category.name),
        );
      },
      child: Container(
        height: 100,
        margin: EdgeInsets.only(top: 16.0),
        decoration: BoxDecoration(
            color: CupertinoColors.white,
            borderRadius: BorderRadius.all(kBorderRadius)),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      category.name,
                      style: kCategoryTitleTextStyle,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      category.description,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: kCategoryDescriptionTextStyle,
                    )
                  ],
                ),
              ),
            ),
            Flexible(
              flex: 1,
              child: CachedNetworkImage(
                height: 100,
                width: 100,
                fit: BoxFit.cover,
                placeholder: (context, url) => CupertinoActivityIndicator(),
                imageUrl: kUrl + category.image,
              ),
            )
          ],
        ),
      ),
    );
  }
}
