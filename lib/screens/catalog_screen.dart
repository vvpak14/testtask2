import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_doover/blocs/catalog/catalog_bloc.dart';
import 'package:test_doover/models/catalog_screen_arguments.dart';
import 'package:test_doover/repositories/catalog_repository.dart';
import 'package:test_doover/widgets/catalog_cart.dart';
import 'package:test_doover/widgets/error_screen.dart';

class CatalogScreen extends StatefulWidget {
  static const routeName = '/catalog';

  @override
  _CatalogScreenState createState() => _CatalogScreenState();
}

class _CatalogScreenState extends State<CatalogScreen>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  final CatalogRepository _catalogRepository = CatalogRepository();

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
    );
  }

  @override
  Widget build(BuildContext context) {
    final CatalogScreenArguments arguments =
        ModalRoute.of(context).settings.arguments;
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text(arguments.category),
      ),
      child: BlocBuilder<CatalogBloc, CatalogState>(
        builder: (context, state) {
          if (state is CatalogLoading) {
            return const CupertinoActivityIndicator();
          }

          if (state is CatalogLoaded) {
            return SafeArea(
              child: ListView.separated(
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(
                    height: 10,
                  );
                },
                padding: EdgeInsets.all(16.0),
                itemCount: state.catalogItems.length,
                itemBuilder: (context, index) {
                  var catalog = state.catalogItems.elementAt(index);
                  return CatalogCard(
                    catalog: catalog,
                    key: UniqueKey(),
                  );
                },
              ),
            );
          }

          if (state is CatalogError) {
            return ErrorScreen(
              errorText: state.errorText,
            );
          }

          return ErrorScreen(
            errorText: 'Unknown error',
          );
        },
      ),
    );
  }
}
