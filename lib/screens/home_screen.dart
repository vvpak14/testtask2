import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_doover/blocs/authorization/authorization_bloc.dart';
import 'package:test_doover/screens/cart_screen.dart';
import 'package:test_doover/screens/catalog_screen.dart';
import 'package:test_doover/screens/categories_screen.dart';
import 'package:test_doover/screens/login_screen.dart';
import 'package:test_doover/screens/profile_screen.dart';

class HomeScreen extends StatefulWidget {
  static const routeName = '/home';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final CupertinoTabController _controller = CupertinoTabController();
  int tabIndex = 0;

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthorizationBloc, AuthorizationState>(
      listener: (context, state) {
        if (state is AuthorizationUnauthorized) {
          Navigator.popAndPushNamed(context, LoginScreen.routeName);
        }
      },
      child: CupertinoTabScaffold(
        tabBar: CupertinoTabBar(
          items: [
            BottomNavigationBarItem(
              icon: const Icon(Icons.bubble_chart),
              title: const Text('Прачечная'),
            ),
            BottomNavigationBarItem(
              icon: const Icon(CupertinoIcons.profile_circled),
              title: const Text('Профиль'),
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.shopping_cart),
              title: const Text('Корзина'),
            ),
          ],
        ),
        tabBuilder: (context, index) {
          if (index == 0) {
            return CupertinoTabView(
              onGenerateRoute: (settings) {
                print('[Route name] ${settings.name}');
                switch (settings.name) {
                  case '/':
                    return CupertinoPageRoute(
                      builder: (context) => CategoriesScreen(),
                      settings: settings,
                    );
                  case '/profile':
                    return CupertinoPageRoute(
                      builder: (context) => ProfileScreen(),
                      settings: settings,
                    );
                }
              },
              routes: {
                CategoriesScreen.routeName: (context) => CategoriesScreen(),
                CatalogScreen.routeName: (context) => CatalogScreen(),
              },
            );
          }
          if (index == 1) {
            return ProfileScreen();
          }
          if (index == 2) {
            return CartScreen();
          }
        },
      ),
    );
  }
}
