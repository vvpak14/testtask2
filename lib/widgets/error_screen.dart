import 'package:flutter/cupertino.dart';

class ErrorScreen extends StatelessWidget {
  final String errorText;

  ErrorScreen({@required this.errorText})
      : assert(errorText != null),
        assert(errorText != '');

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text(errorText),
      ),
    );
  }
}
