import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_doover/blocs/catalog/catalog_bloc.dart';
import 'package:test_doover/models/catalog_item.dart';

import '../constants.dart';

class CatalogCard extends StatelessWidget {
  const CatalogCard({
    Key key,
    @required this.catalog,
  }) : super(key: key);

  final CatalogItem catalog;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            color: CupertinoColors.white,
            borderRadius: BorderRadius.all(kBorderRadius),
          ),
          padding: const EdgeInsets.all(16.0),
          child: Row(
            children: [
              Flexible(
                flex: 1,
                child: CachedNetworkImage(
                  placeholder: (context, url) => CupertinoActivityIndicator(),
                  imageUrl: catalog.image,
                ),
              ),
              Flexible(
                flex: 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      catalog.name,
                      style: kCatalogTitleTextStyle,
                    ),
                    RichText(
                      text: TextSpan(
                        children: [
                          const TextSpan(
                            text: 'Срок чистки / ',
                            style: kCategoryDescriptionTextStyle,
                          ),
                          TextSpan(
                            text: '${catalog.unitTime} дня',
                            style: kCatalogCountTextStyle,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 15.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                BlocProvider.of<CatalogBloc>(context).add(
                                    CatalogIncreaseCount(uuid: catalog.uuid));
                              },
                              child: Icon(
                                CupertinoIcons.add_circled,
                              ),
                            ),
                            (catalog.count != null && catalog.count > 0)
                                ? Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 8.0),
                                        child: Text(
                                          catalog.count.toString(),
                                          style: const TextStyle(
                                            color: CupertinoColors.activeBlue,
                                          ),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          BlocProvider.of<CatalogBloc>(context)
                                              .add(CatalogDecreaseCount(
                                                  uuid: catalog.uuid));
                                        },
                                        child: Icon(
                                          CupertinoIcons.minus_circled,
                                        ),
                                      ),
                                    ],
                                  )
                                : Container(),
                          ],
                        ),
                        Text(
                          '${catalog.unitPrice} тг',
                          style: kCatalogTitleTextStyle,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Positioned(
          top: 0,
          right: 0,
          child: GestureDetector(
            onTap: () {
              print('?');
            },
            child: Container(
              padding: const EdgeInsets.all(8.0),
              decoration: const BoxDecoration(
                color: CupertinoColors.systemGrey,
                borderRadius: BorderRadius.only(
                  topRight: kBorderRadius,
                  bottomLeft: kBorderRadius,
                ),
              ),
              child: const Icon(
                Icons.live_help,
                color: CupertinoColors.white,
                size: 15.0,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
