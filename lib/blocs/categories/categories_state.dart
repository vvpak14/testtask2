part of 'categories_bloc.dart';

abstract class CategoriesState extends Equatable {
  const CategoriesState();
}

class CategoriesInitial extends CategoriesState {
  @override
  List<Object> get props => [];
}

class CategoriesLoading extends CategoriesState {
  @override
  List<Object> get props => [];
}

class CategoriesLoaded extends CategoriesState {
  final List<CategoryModel> categories;

  const CategoriesLoaded({
    @required this.categories,
  }) : assert(categories != null);

  @override
  List<Object> get props => [categories];
}

class CategoriesError extends CategoriesState {
  final String errorText;

  const CategoriesError({
    @required this.errorText,
  })  : assert(errorText != null),
        assert(errorText != '');

  @override
  List<Object> get props => [errorText];
}
