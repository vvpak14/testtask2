import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:test_doover/blocs/authorization/authorization_bloc.dart';
import 'package:test_doover/models/category_model.dart';
import 'package:test_doover/repositories/categories_repository.dart';

part 'categories_event.dart';
part 'categories_state.dart';

class CategoriesBloc extends Bloc<CategoriesEvent, CategoriesState> {
  final AuthorizationBloc authorizationBloc;
  final CategoriesRepository categoriesRepository;
  CategoriesBloc({
    @required this.categoriesRepository,
    @required this.authorizationBloc,
  })  : assert(categoriesRepository != null),
        super(CategoriesInitial());

  @override
  Stream<CategoriesState> mapEventToState(
    CategoriesEvent event,
  ) async* {
    var token = authorizationBloc.state is AuthorizationAuthorized
        ? (authorizationBloc.state as AuthorizationAuthorized).token.key
        : null;
    if (event is CategoriesLoad) {
      yield* _mapCategoriesLoadToState(event, token);
    }
  }

  Stream<CategoriesState> _mapCategoriesLoadToState(
    CategoriesLoad event,
    String token,
  ) async* {
    yield (CategoriesLoading());
    try {
      var categories = await categoriesRepository.loadCategories(token: token);
      yield (CategoriesLoaded(categories: categories));
    } catch (error) {
      yield (CategoriesError(errorText: error.toString()));
    }
  }
}
