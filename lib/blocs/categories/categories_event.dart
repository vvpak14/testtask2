part of 'categories_bloc.dart';

abstract class CategoriesEvent extends Equatable {
  const CategoriesEvent();
}

class CategoriesLoad extends CategoriesEvent {
  @override
  List<Object> get props => [];
}

class CategoriesRefresh extends CategoriesEvent {
  @override
  List<Object> get props => [];
}
