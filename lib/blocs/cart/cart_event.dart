part of 'cart_bloc.dart';

abstract class CartEvent extends Equatable {
  const CartEvent();
}

class CartItemsLoad extends CartEvent {
  @override
  List<Object> get props => [];
}

class CartItemAdd extends CartEvent {
  final CatalogItem newCatalogItem;

  const CartItemAdd({@required this.newCatalogItem})
      : assert(newCatalogItem != null);

  @override
  List<Object> get props => [newCatalogItem];
}

class CartItemRemove extends CartEvent {
  final CatalogItem removingCatalogItem;

  const CartItemRemove({@required this.removingCatalogItem})
      : assert(removingCatalogItem != null);

  @override
  List<Object> get props => [removingCatalogItem];
}
