part of 'cart_bloc.dart';

abstract class CartState extends Equatable {
  const CartState();
}

class CartInitial extends CartState {
  @override
  List<Object> get props => [];
}

class CartLoading extends CartState {
  @override
  List<Object> get props => [];
}

class CartLoaded extends CartState {
  final List<CatalogItem> selectedCatalogs;

  const CartLoaded({
    @required this.selectedCatalogs,
  });

  @override
  List<Object> get props => [selectedCatalogs];
}

class CartError extends CartState {
  final String errorText;

  const CartError({@required this.errorText})
      : assert(errorText != null),
        assert(errorText != '');

  @override
  List<Object> get props => [errorText];
}
