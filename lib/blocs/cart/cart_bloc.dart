import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:test_doover/models/catalog_item.dart';
import 'package:test_doover/repositories/cart_repository.dart';

part 'cart_event.dart';
part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  final CartRepository cartRepository;

  CartBloc({@required this.cartRepository})
      : assert(cartRepository != null),
        super(CartInitial());

  @override
  Stream<CartState> mapEventToState(
    CartEvent event,
  ) async* {
    if (event is CartItemsLoad) {
      yield* _mapCartItemLoadToState(event);
    }

    if (event is CartItemAdd) {
      yield* _mapCartItemAddToState(
          event, (state is CartLoaded) ? state : null);
    }

    if (event is CartItemRemove) {}
  }

  Stream<CartState> _mapCartItemLoadToState(CartItemsLoad event) async* {
    yield (CartLoading());
    try {
      var cart = await cartRepository.loadCart();
      yield (CartLoaded(selectedCatalogs: cart));
    } catch (error) {
      yield (CartError(errorText: error.toString()));
    }
  }

  Stream<CartState> _mapCartItemAddToState(
    CartItemAdd event,
    CartLoaded state,
  ) async* {
    var element = state.selectedCatalogs
        .firstWhere((element) => element.uuid == event.newCatalogItem.uuid);

    if (element != null) {
      if (element.count == null) {
        element.count = 0;
      } else {
        element.count += 1;
      }
    } else {
      var newCart =
          (state != null) ? state.selectedCatalogs : List<CatalogItem>();
    }
    var newCart =
        (state != null) ? state.selectedCatalogs : List<CatalogItem>();
    newCart.add(event.newCatalogItem);
    yield (CartLoaded(selectedCatalogs: newCart));
    try {
      cartRepository.persistCart(cart: newCart);
    } catch (_) {}
  }
}
