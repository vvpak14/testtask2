part of 'authorization_bloc.dart';

abstract class AuthorizationState extends Equatable {
  const AuthorizationState();
}

class AuthorizationUnauthorized extends AuthorizationState {
  @override
  List<Object> get props => [];
}

class AuthorizationLoading extends AuthorizationState {
  @override
  List<Object> get props => [];
}

class AuthorizationAuthorized extends AuthorizationState {
  final Token token;
  final Profile profile;

  const AuthorizationAuthorized({
    @required this.token,
    @required this.profile,
  })  : assert(profile != null),
        assert(token != null);

  @override
  List<Object> get props => [token, profile];
}
