import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:test_doover/models/profile.dart';
import 'package:test_doover/models/token.dart';
import 'package:test_doover/repositories/user_repository.dart';

part 'authorization_event.dart';
part 'authorization_state.dart';

class AuthorizationBloc extends Bloc<AuthorizationEvent, AuthorizationState> {
  UserRepository userRepository;

  AuthorizationBloc({@required this.userRepository})
      : assert(userRepository != null),
        super(AuthorizationUnauthorized());

  @override
  Stream<AuthorizationState> mapEventToState(
    AuthorizationEvent event,
  ) async* {
    if (event is AuthorizationAuthorize) {
      yield* _mapAuthorizationAuthorize(event);
    }

    if (event is AuthorizationAppStart) {
      yield* _mapAuthorizationAppStartToState(event);
    }

    if (event is AuthorizationLogout) {
      yield* _mapAuthorizationLogoutToState();
    }
  }

  Stream<AuthorizationState> _mapAuthorizationLogoutToState() async* {
    try {
      await userRepository.deleteUserData();
      yield (AuthorizationUnauthorized());
    } catch (_) {}
  }

  Stream<AuthorizationState> _mapAuthorizationAuthorize(
      AuthorizationAuthorize event) async* {
    yield (AuthorizationLoading());
    var isTokenValid = await userRepository.checkToken(event.token);
    var profile = await userRepository.loadProfile(event.token);
    if (isTokenValid) {
      yield (AuthorizationAuthorized(token: event.token, profile: profile));
    } else {
      yield (AuthorizationUnauthorized());
    }
  }

  Stream<AuthorizationState> _mapAuthorizationAppStartToState(
      AuthorizationAppStart event) async* {
    yield (AuthorizationLoading());
    try {
      var token = await userRepository.loadToken();
      add(AuthorizationAuthorize(token: token));
    } catch (error) {
      yield (AuthorizationUnauthorized());
    }
  }
}
