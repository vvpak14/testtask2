part of 'authorization_bloc.dart';

abstract class AuthorizationEvent extends Equatable {
  const AuthorizationEvent();
}

class AuthorizationAuthorize extends AuthorizationEvent {
  final Token token;

  const AuthorizationAuthorize({@required this.token});

  @override
  List<Object> get props => [token];
}

class AuthorizationAppStart extends AuthorizationEvent {
  @override
  List<Object> get props => [];
}

class AuthorizationLogout extends AuthorizationEvent {
  @override
  List<Object> get props => [];
}
