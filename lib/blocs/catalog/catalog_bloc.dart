import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:test_doover/blocs/authorization/authorization_bloc.dart';
import 'package:test_doover/models/catalog_item.dart';
import 'package:test_doover/repositories/catalog_repository.dart';

part 'catalog_event.dart';
part 'catalog_state.dart';

class CatalogBloc extends Bloc<CatalogEvent, CatalogState> {
  final CatalogRepository catalogRepository;
  final AuthorizationBloc authorizationBloc;
  CatalogBloc({
    @required this.catalogRepository,
    @required this.authorizationBloc,
  })  : assert(catalogRepository != null),
        assert(authorizationBloc != null),
        super(CatalogInitial());

  @override
  Stream<CatalogState> mapEventToState(
    CatalogEvent event,
  ) async* {
    var token = authorizationBloc.state is AuthorizationAuthorized
        ? (authorizationBloc.state as AuthorizationAuthorized).token.key
        : null;

    if (event is CatalogLoad) {
      yield* _mapCatalogLoadToState(event, token);
    }

    if (event is CatalogIncreaseCount) {
      yield* _mapCatalogIncreaseCountToState(event);
    }

    if (event is CatalogDecreaseCount) {
      yield* _mapCatalogDecreaseCountToState(event);
    }

    if (event is CatalogResetCounts) {
      yield* _mapCatalogClearCountToState();
    }
  }

  Stream<CatalogState> _mapCatalogDecreaseCountToState(
      CatalogDecreaseCount event) async* {
    yield (CatalogLoading());
    if (state is CatalogLoaded) {
      var newCatalogs = (state as CatalogLoaded).catalogItems.map((e) {
        if (e.uuid == event.uuid) {
          if (e.count == null) {
            e.count = 0;
          } else {
            e.count -= 1;
          }
          print(e.uuid);
          print(e.count);
        }
        return e;
      }).toList();

      yield (CatalogLoaded(catalogItems: newCatalogs));
    }
  }

  Stream<CatalogState> _mapCatalogClearCountToState() async* {
    yield (CatalogLoading());
    if (state is CatalogLoaded) {
      var newCatalogs = (state as CatalogLoaded).catalogItems.map((e) {
        e.count = 0;
        return e;
      }).toList();

      yield (CatalogLoaded(catalogItems: newCatalogs));
    }
  }

  Stream<CatalogState> _mapCatalogIncreaseCountToState(
      CatalogIncreaseCount event) async* {
    yield (CatalogLoading());
    if (state is CatalogLoaded) {
      var newCatalogs = (state as CatalogLoaded).catalogItems.map((e) {
        if (e.uuid == event.uuid) {
          if (e.count == null) {
            e.count = 1;
          } else {
            e.count += 1;
          }
          print(e.uuid);
          print(e.count);
        }
        return e;
      }).toList();

      yield (CatalogLoaded(catalogItems: newCatalogs));
    }
  }

  Stream<CatalogState> _mapCatalogLoadToState(
    CatalogLoad event,
    String token,
  ) async* {
    yield (CatalogLoading());
    try {
      var catalogs = await catalogRepository.loadCatalogs(token: token);
      yield (CatalogLoaded(catalogItems: catalogs));
    } catch (error) {
      yield (CatalogError(errorText: error.toString()));
    }
  }
}
