part of 'catalog_bloc.dart';

abstract class CatalogState extends Equatable {
  const CatalogState();
}

class CatalogInitial extends CatalogState {
  @override
  List<Object> get props => [];
}

class CatalogLoading extends CatalogState {
  @override
  List<Object> get props => [];
}

class CatalogLoaded extends CatalogState {
  final List<CatalogItem> catalogItems;

  const CatalogLoaded({@required this.catalogItems})
      : assert(catalogItems != null);

  @override
  List<Object> get props => [catalogItems];
}

class CatalogError extends CatalogState {
  final String errorText;

  const CatalogError({@required this.errorText})
      : assert(errorText != null),
        assert(errorText != '');

  @override
  List<Object> get props => [errorText];
}
