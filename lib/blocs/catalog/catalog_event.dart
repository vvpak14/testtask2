part of 'catalog_bloc.dart';

abstract class CatalogEvent extends Equatable {
  const CatalogEvent();
}

class CatalogLoad extends CatalogEvent {
  @override
  List<Object> get props => [];
}

class CatalogRefresh extends CatalogEvent {
  @override
  List<Object> get props => [];
}

class CatalogIncreaseCount extends CatalogEvent {
  final String uuid;

  CatalogIncreaseCount({@required this.uuid})
      : assert(uuid != null),
        assert(uuid != '');

  @override
  List<Object> get props => [uuid];
}

class CatalogDecreaseCount extends CatalogEvent {
  final String uuid;

  CatalogDecreaseCount({@required this.uuid})
      : assert(uuid != null),
        assert(uuid != '');

  @override
  List<Object> get props => [uuid];
}

class CatalogResetCounts extends CatalogEvent {
  @override
  List<Object> get props => [];
}
