import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:test_doover/blocs/authorization/authorization_bloc.dart';
import 'package:test_doover/repositories/user_repository.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  UserRepository userRepository;
  AuthorizationBloc authorizationBloc;

  LoginBloc({
    @required this.authorizationBloc,
    @required this.userRepository,
  })  : assert(authorizationBloc != null),
        assert(userRepository != null),
        super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is LoginButtonPress) {
      yield* _mapLoginButtonPressedToState(event);
    }
  }

  Stream<LoginState> _mapLoginButtonPressedToState(
      LoginButtonPress event) async* {
    yield (LoginLoading());
    try {
      var token = await userRepository.authenticate(
        username: event.username,
        password: event.password,
      );
      authorizationBloc.add(AuthorizationAuthorize(token: token));
      yield (LoginInitial());
    } catch (error) {
      yield (LoginError(errorString: error.toString()));
    }
  }
}
