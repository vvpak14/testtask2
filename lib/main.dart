import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:test_doover/blocs/authorization/authorization_bloc.dart';
import 'package:test_doover/blocs/cart/cart_bloc.dart';
import 'package:test_doover/blocs/catalog/catalog_bloc.dart';
import 'package:test_doover/blocs/login/login_bloc.dart';
import 'package:test_doover/repositories/cart_repository.dart';
import 'package:test_doover/repositories/catalog_repository.dart';
import 'package:test_doover/repositories/user_repository.dart';
import 'package:test_doover/screens/catalog_screen.dart';
import 'package:test_doover/screens/home_screen.dart';
import 'package:test_doover/screens/login_screen.dart';
import 'package:test_doover/screens/preloader_screen.dart';

class PrintBlocObserver extends BlocObserver {
  @override
  void onChange(Cubit cubit, Change change) {
    super.onChange(cubit, change);
    print(change);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print('[Transition $transition]');
  }
}

void main() {
  Bloc.observer = PrintBlocObserver();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  UserRepository userRepository = UserRepository();
  CartRepository cartRepository = CartRepository();
  CatalogRepository catalogRepository = CatalogRepository();

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => AuthorizationBloc(
            userRepository: userRepository,
          )..add(
              AuthorizationAppStart(),
            ),
        ),
        BlocProvider(
          create: (context) => LoginBloc(
            authorizationBloc: BlocProvider.of<AuthorizationBloc>(context),
            userRepository: userRepository,
          ),
        ),
        BlocProvider(
          create: (context) => CartBloc(
            cartRepository: cartRepository,
          )..add(CartItemsLoad()),
        ),
        BlocProvider(
          create: (context) => CatalogBloc(
            catalogRepository: catalogRepository,
            authorizationBloc: BlocProvider.of<AuthorizationBloc>(context),
          )..add(CatalogLoad()),
        )
      ],
      child: CupertinoApp(
        title: 'Flutter Demo',
        theme: CupertinoThemeData(
          textTheme: CupertinoTextThemeData(
            textStyle: GoogleFonts.getFont('Lato'),
          ),
          scaffoldBackgroundColor: Color(0xFFE5E5E5),
        ),
        routes: {
          '/': (context) => PreloaderScreen(),
          LoginScreen.routeName: (context) => LoginScreen(),
          HomeScreen.routeName: (context) => HomeScreen(),
          CatalogScreen.routeName: (context) => CatalogScreen(),
        },
      ),
    );
  }
}
