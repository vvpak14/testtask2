import 'package:flutter/cupertino.dart';

const kUrl = 'https://api.doover.tech';

//Styles
const kBorderRadius = Radius.circular(10.0);

const kLoginTextInputStyle = TextStyle(
  fontSize: 20.0,
  color: CupertinoColors.black,
);

const kCategoryTitleTextStyle = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 20.0,
  color: Color(0xff172853),
);

const kCategoryDescriptionTextStyle = TextStyle(
  fontWeight: FontWeight.normal,
  color: CupertinoColors.inactiveGray,
);

const kCatalogTitleTextStyle = TextStyle(
  fontWeight: FontWeight.bold,
  color: Color(0xff172853),
);

const kCatalogCountTextStyle = TextStyle(
  fontWeight: FontWeight.bold,
  color: Color(0xff172853),
);
