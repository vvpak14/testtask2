import 'package:equatable/equatable.dart';

import 'category_model.dart';

class CatalogItem extends Equatable {
  String name;
  String image;
  String description;
  String unitType;
  int unitTime;
  double unitPrice;
  CategoryModel category;
  String uuid;
  int count;

  CatalogItem(
      {this.name,
      this.image,
      this.description,
      this.unitType,
      this.unitTime,
      this.unitPrice,
      this.category,
      this.uuid});

  CatalogItem.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    image = json['image'];
    description = json['description'];
    unitType = json['unit_type'];
    unitTime = json['unit_time'];
    unitPrice = double.parse(json['unit_price']);
    category = json['category'] != null
        ? new CategoryModel.fromJson(json['category'])
        : null;
    uuid = json['uuid'];
    count = json['count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['image'] = this.image;
    data['description'] = this.description;
    data['unit_type'] = this.unitType;
    data['unit_time'] = this.unitTime;
    data['unit_price'] = this.unitPrice;
    if (this.category != null) {
      data['category'] = this.category.toJson();
    }
    data['uuid'] = this.uuid;
    data['count'] = this.count;
    return data;
  }

  @override
  List<Object> get props => [
        name,
        image,
        description,
        unitType,
        unitTime,
        unitPrice,
        category,
        uuid,
        count,
      ];
}
