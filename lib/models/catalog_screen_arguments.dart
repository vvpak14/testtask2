import 'package:flutter/foundation.dart';

class CatalogScreenArguments {
  final String category;

  const CatalogScreenArguments({@required this.category});
}
