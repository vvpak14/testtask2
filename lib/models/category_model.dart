class CategoryModel {
  String name;
  String image;
  String description;
  String uuid;

  CategoryModel({this.name, this.image, this.description, this.uuid});

  CategoryModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    image = json['image'];
    description = json['description'];
    uuid = json['uuid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['image'] = this.image;
    data['description'] = this.description;
    data['uuid'] = this.uuid;
    return data;
  }
}
