import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_doover/constants.dart';
import 'package:test_doover/models/profile.dart';
import 'package:test_doover/models/token.dart';

class UserRepository {
  Future<Token> authenticate({
    @required String username,
    @required String password,
  }) async {
    var response = await http.post('$kUrl/auth/login/', body: {
      "username": username,
      "password": password,
    });

    if (response.statusCode == 200) {
      persistToken(response.body);
      return Token.fromJson(json.decode(response.body));
    }

    throw Exception(response.body);
  }

  Future<void> persistToken(String tokenString) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('token', tokenString);
  }

  Future<Token> loadToken() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var tokenString = preferences.getString('token');
    return Token.fromJson(json.decode(tokenString));
  }

  Future<Profile> loadProfile(Token token) async {
    var response =
        await http.get('$kUrl/me/', headers: {'Authorization': 'Token $token'});

    if (response.statusCode == 200) {
      return Profile.fromJson(json.decode(response.body));
    }

    return null;
  }

  Future<void> deleteUserData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.remove('token');
    return;
  }

  Future<bool> checkToken(Token token) async {
    if (loadProfile(token) == null) {
      return false;
    }
    return true;
  }
}
