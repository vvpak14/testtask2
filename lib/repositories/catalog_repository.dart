import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:test_doover/constants.dart';
import 'package:test_doover/models/catalog_item.dart';

class CatalogRepository {
  Future<List<CatalogItem>> loadCatalogs({
    String token,
  }) async {
    var response = await http.get('$kUrl/catalog/', headers: {
      'Authorization': 'Token $token',
      'Content-Type': 'application/json',
    });

    if (response.statusCode == 200) {
      var catalogs = List<CatalogItem>();

      var jsonObject = json.decode(utf8.decode(response.bodyBytes));

      jsonObject.forEach((catalog) {
        catalogs.add(CatalogItem.fromJson(catalog));
      });

      return catalogs;
    }

    throw Exception(response.body);
  }
}
