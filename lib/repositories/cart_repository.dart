import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_doover/models/catalog_item.dart';

class CartRepository {
  Future<List<CatalogItem>> loadCart() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    // var cartString = preferences.getString('cart');
    var cartString;

    if (cartString == null) {
      return [];
    }

    var jsonObject = json.decode(cartString);
    var cart = List<CatalogItem>();
    jsonObject.forEach((catalogItem) {
      cart.add(CatalogItem.fromJson(catalogItem));
    });

    return cart;
  }

  Future<void> persistCart({@required List<CatalogItem> cart}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('cart', json.encode(cart));
  }
}
