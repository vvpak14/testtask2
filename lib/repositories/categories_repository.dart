import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:test_doover/constants.dart';
import 'package:test_doover/models/category_model.dart';

class CategoriesRepository {
  Future<List<CategoryModel>> loadCategories({
    @required String token,
  }) async {
    var response = await http.get(
      '$kUrl/catalog/categories',
      headers: {
        'Authorization': 'Token $token',
        'Content-Type': 'application/json',
      },
    );

    if (response.statusCode == 200) {
      var categories = List<CategoryModel>();

      var jsonObject = json.decode(utf8.decode(response.bodyBytes));

      jsonObject.forEach((category) {
        categories.add(CategoryModel.fromJson(category));
      });

      return categories;
    }

    throw Exception(response.body);
  }
}
